import Head from 'next/head'
import Button from "@material-tailwind/react/Button";
import Capa from '../components/Capa';
import Section from '../components/Section';
import Footer from '../components/Footer';

export default function Home() {
  return (
    <div className="flex flex-col items-center justify-center w-screen">
      <Head>
        <title>Salão do Milhão - Salonflix</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <Capa />
      <main className="">


        
        <h3 className="py-12 font-bold text-4xl">Serviços</h3>

        <div className="space-y-12 p-2">
          <Section />
          <Section />
          <Section />
        </div>
      </main>

      <Footer />
      
    </div>
  )
}
