import React from 'react'

// const Servico = () => {
//   <div>
//     <h1>1</h1>
//   </div>
// }


const Servicos = () => {
  return (
    <div className="border border-indigo-700 p-8 rounded grid space-y-4 bg-cards">

      <div className="w-full flex justify-between gap-4">
        <img src="/barba.png" alt="" />
        <div>
          <h3 className="text-3xl font-semibold">Barba + Corte</h3>
          <p>Promoção Corte Combo Exclusivamente para os horários:</p>
          <p>Segunda a quarta-feira das 10h as 16h</p>
        </div>
      </div>
      
      
        <div>
          <button className="btn">
            <p className="text-2xl">1 X ao mês - R$ 80,75</p>
            <p className="text-3xl font-bold">10% OFF</p>
          </button>
        </div>
        <div>
          <button className="btn">
            <p className="text-2xl">2 X ao mês - R$ 80,75</p>
            <p className="text-3xl font-bold">15% OFF</p>
          </button>
        </div>
        <div>
          <button className="btn">
            <p className="text-2xl">3 X ao mês - R$ 80,75</p>
            <p className="text-3xl font-bold">20% OFF</p>
          </button>
        </div>
        <div>
          <button className="btn">
            <p className="text-2xl">4 X ao mês - R$ 80,75</p>
            <p className="text-3xl font-bold">25% OFF</p>
          </button>
        </div>

      <div>
        
      </div>
    </div>
  )
}

function Section() {
  return (
    <div className="p-2">

      <Servicos />
    </div>
  )
}

export default Section
