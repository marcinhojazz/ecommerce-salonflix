import React from 'react'
// import '/styles/tailwind.css'

function Footer() {
  return (
    <div className="flex justify-between w-full border border-blue-900 p-12">
      <p className="">Salão do Milhão - Rua das Flores, 22 - (12)3456-7890</p>
      <div className="flex justify-between gap-8">
        <span>
          <a className="text-blue-700 hover:text-blue-300" href="">Termos de Uso</a>
        </span>
        <span>
          <a className="" href="">Política de privacidade</a>
        </span>
      </div>
    </div>
  )
}

export default Footer
